{{/*
Expand the name of the chart.
*/}}
{{- define "btcTxMailAlert.name" -}}
{{- default (printf "%s-%s" .Release.Name "btc-tx-mail-alert") .Values.service.name | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "btcTxMailAlert.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Definition of default metadata lables
*/}}
{{- define "btcTxMailAlert.metadata.labels"}}  labels:
    name: {{ template "btcTxMailAlert.name" . }}
    app: {{ template "btcTxMailAlert.name" . }}
    chart: {{ template "btcTxMailAlert.chart" . }}
    release: {{ .Release.Name }}
{{- end -}}
