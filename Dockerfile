FROM golang:1.15-alpine AS builder

COPY ./go /src

WORKDIR /src

RUN apk --no-cache add ca-certificates

RUN go mod tidy && \
    GOARCH=amd64 \
    GOOS=linux \
    CGO_ENABLED=0 \
    go build -a -ldflags '-w -s -extldflags "-static"' -installsuffix cgo -o btc-tx-mail-alert .

FROM scratch

WORKDIR /app
COPY --from=builder /src/btc-tx-mail-alert .
COPY --from=builder /src/config.yaml ./config.yaml
COPY --from=builder /src/email-template.html ./email-template.html
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/

CMD ["./btc-tx-mail-alert"]
