# bitcoin transaction mail alert

A small application, that checks the the transactions for a configured Bitcoin Address. 
Uses the (Blockcypher API)[https://www.blockcypher.com/dev/bitcoin] to get the Bitcoin Data.


## build

### application

* Go to `./go`
* Run `go build .` -> go will create you a binary based on your current system.

### docker

To build the application, you can simply run a `docker build .` and tag the image after your preferred naming convention.
For example `docker build -t hub.docker.com/repo/to/btc-tx-mail-alert:0.1.0 .`

The docker build will also build the go binary. This way is not dependant on the plain application build.

### helm

To build the Helm Chart you can use a simple `helm package ./helm` from the root directory.

## deploy

### plain application

Take the executable generated by the build, and put it anywhere you want. 
you just need to make sure that the

* config.yaml
* email-template.html

are in the same folder as the executable. 

also the application needs to have the permissions to create the path `./storage/blockheight.txt`

You can now the application with a CronJob or manual, you are free to decide.

### docker 

Build the docker image, then put it in your favourite docker registry.
Or just leave it as a local image that your machine can use it.

That the application works in plain docker you just need to override the `config.yaml` with a mount.
that mount should contain a proper configuration not the example values.

for persistence you need to mount the image path `/app/storage`.

###  helm

You can either packe the chart with `helm package ./helm` (from the root directory)
and put it in your favourite helm repository.
Or if you have a already configured kubeconfig, you can simply install it with `helm install ./helm`

You maybe want to adjust the values to fit your needs. You can do this by editing the values that are in the helm folder, or you make your own values file. 
Your can install with a own values file like this: `helm install ./helm -f path/to/your/values`

Especially you need to adjust the stuff in the image sections, since this is dependant on how you tag the image.

For a Reference of the aviable configuaration Options and a short explanation, see the (Helm Readme)[./helm/Readme.md]

## Recommended Infrastructure

The Recommended Infrastructure is a Kubernetes Cluster with Version 1.16. Other Versions of Kubernetes should also work, unless they do not support the following Resource Apis

* ConfigMap v1
* CronJob batch/v1beta1

It is also recommended to provide a PersistenVolume and according PersistentVolumeClaim with atleast a few Mebibytes for persistence. You can configure the PVC Name for the App over the Helm Chart Values (`persistence.pvcName`).