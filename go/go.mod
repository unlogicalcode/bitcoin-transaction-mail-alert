module bitcoin-transaction-mail-alert

go 1.15

require (
	github.com/blockcypher/gobcy v2.0.1+incompatible
	github.com/btcsuite/btcd v0.21.0-beta // indirect
	gopkg.in/yaml.v2 v2.3.0
)
