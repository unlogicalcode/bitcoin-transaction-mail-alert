package main

import (
	"fmt"
	"io/ioutil"

	"gopkg.in/yaml.v2"
)

type Config struct {
	Bitcoin BitcoinConfig `yaml:"bitcoin"`
	Mail    MailConfig    `yaml:"mail"`
}

type BitcoinConfig struct {
	Address               string  `yaml:"address"`
	NotificationThreshold float64 `yaml:"notificationThreshold"`
}

type MailConfig struct {
	SMTP       SmtpConfig `yaml:"smtp"`
	Recipients []string   `yaml:"recipients"`
}

type SmtpConfig struct {
	Host     string `yaml:"host"`
	Port     int    `yaml:"port"`
	Password string `yaml:"password"`
	From     string `yaml:"fromEmail"`
}

func GetConfig() *Config {
	configFile, err := ioutil.ReadFile("./config.yaml")
	if err != nil {
		fmt.Println(err)
	}

	c := &Config{}
	err = yaml.Unmarshal(configFile, c)
	if err != nil {
		fmt.Println(err)
	}

	fmt.Println("loaded config")

	return c
}
