package main

import (
	"bytes"
	"fmt"
	"net/smtp"
	"strconv"
	"text/template"
)

type MessageParams struct {
	BitcoinAddress   string
	BlockHeight      int
	TransactionHash  string
	TransactionValue float64
}

func SendMail(messageParams MessageParams, config MailConfig) {

	auth := smtp.PlainAuth("", config.SMTP.From, config.SMTP.Password, config.SMTP.Host)

	mailContent, err := template.ParseFiles("./email-template.html")

	var body bytes.Buffer

	mimeHeaders := "MIME-version: 1.0;\nContent-Type: text/html; charset=\"UTF-8\";\n\n"
	body.Write([]byte(fmt.Sprintf("Subject: Bitcoin Transaction Alert \n%s\n\n", mimeHeaders)))

	mailContent.Execute(&body, messageParams)

	url := config.SMTP.Host + ":" + strconv.Itoa(config.SMTP.Port)
	from := config.SMTP.From
	recipients := config.Recipients
	fmt.Println("sending mail for tx: " + messageParams.TransactionHash)
	err = smtp.SendMail(url, auth, from, recipients, body.Bytes())
	//err := smtp.SendMail(url, auth, config.SMTP.From, config.Recipients, body.Bytes())
	if err != nil {
		fmt.Println(err)
		return
	}
}
