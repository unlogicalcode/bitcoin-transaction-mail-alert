package main

import (
	"fmt"
	"strconv"

	"github.com/blockcypher/gobcy"
)

func main() {

	bitcoin := gobcy.API{}
	bitcoin.Coin = "btc"
	bitcoin.Chain = "main"

	config := GetConfig()

	address, err := bitcoin.GetAddr(config.Bitcoin.Address, nil)

	if err != nil {
		fmt.Println(err)
		return
	}

	previousLatestBlockHeight := GetLastBlockHeight()

	var latestProcessedBlockHeight int
	var count int

	for i, transaction := range address.TXRefs {

		if transaction.BlockHeight > previousLatestBlockHeight {
			valueBTC := float64(transaction.Value.Int64()) * 0.00000001

			if valueBTC > config.Bitcoin.NotificationThreshold {
				message := MessageParams{BitcoinAddress: transaction.Address,
					BlockHeight:      transaction.BlockHeight,
					TransactionHash:  transaction.TXHash,
					TransactionValue: valueBTC}

				SendMail(message, config.Mail)
			}

			if transaction.BlockHeight > latestProcessedBlockHeight {
				latestProcessedBlockHeight = transaction.BlockHeight
			}
		}
		count = i
	}

	fmt.Println("Processed " + strconv.Itoa(count) + " transactions for " + address.Address)

	SaveLastBlockHeight(latestProcessedBlockHeight)
}
