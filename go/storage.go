package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"strconv"
	"strings"
)

var storagePath = "./storage/blockheight.txt"

func GetLastBlockHeight() int {
	contentBytes, err := ioutil.ReadFile(storagePath)
	if err != nil {
		fmt.Println(err)
		return -1
	}

	content := string(contentBytes)
	lastBlockHeight, err := strconv.Atoi(strings.Split(content, " ")[1])
	if err != nil {
		fmt.Println(err)
	}

	fmt.Println("start from block height: " + strings.Split(content, " ")[1])
	return lastBlockHeight
}

func SaveLastBlockHeight(lastBlockHeight int) {

	os.Remove(storagePath)
	file, err := os.Create(storagePath)
	if err != nil {
		fmt.Println(err)
		return
	}

	file.WriteString("lastBlockHeight: " + strconv.Itoa(lastBlockHeight))
	fmt.Println("saving last block height" + strconv.Itoa(lastBlockHeight))
}
